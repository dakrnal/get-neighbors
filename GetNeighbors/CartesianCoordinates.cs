﻿using System;

namespace GetNeighbors
{
    public static class CartesianCoordinates
    {
        /// <summary>
        /// Gets from a set of points only points that are h-neighbors for a point with integer coordinates x and y.
        /// </summary>
        /// <param name="point">Given point with integer coordinates x and y.</param>
        /// <param name="h">Distance around a given point.</param>
        /// <param name="points">A given set of points.</param>
        /// <returns>Only points that are h-neighbors for a point with integer coordinates x and y.</returns>
        /// <exception cref="ArgumentNullException">Throw when array points is null.</exception>
        /// <exception cref="ArgumentException">Throw when h-distance is less or equals zero.</exception>
        public static Point[] GetNeighbors(Point point, int h, params Point[]? points)
        {
            if (points is null)
            {
                throw new ArgumentNullException(nameof(points));
            }

            if (h < 0)
            {
                throw new ArgumentException("Range cannot be less or equals zero.");
            }

            int k = 0;
            for (int i = 0; i < points.Length; i++)
            {
                Point p = points[i];
                if (Math.Abs(p.X - point.X) <= h && Math.Abs(p.Y - point.Y) <= h)
                {
                    k++;
                }
            }

            Point[] result = new Point[k];
            k = 0;
            for (int i = 0; i < points.Length; i++)
            {
                Point p = points[i];
                if (Math.Abs(p.X - point.X) <= h && Math.Abs(p.Y - point.Y) <= h)
                {
                    result[k] = p;
                    k++;
                }
            }

            return result;
        }
    }
}
